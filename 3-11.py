import matplotlib.pyplot as plt
import numpy as np
 
fig, ax = plt.subplots(nrows=3, ncols=3, sharex=True, sharey=True)
 
sin, cos = np.sin, np.cos
 
points = np.array([[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]])
ones = np.ones(len(points))
A = np.concatenate([points, np.expand_dims(ones, axis=1)], axis=1)
theta = np.pi / 4
 
transformations = [
    [
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1],
    ],
    [[1, 0, 0.5], [0, 1, 0.5], [0, 0, 1]],
    [[-1, 0, 0], [0, 1, 0], [0, 0, 1]],
    [[1, 0, 0], [0, -1, 0], [0, 0, 1]],
    [[2, 0, 0], [0, 1, 0], [0, 0, 1]],
    [[1, 0, 0], [0, 2, 0], [0, 0, 1]],
    [[cos(theta), -sin(theta), 0], [sin(theta), cos(theta), 0], [0, 0, 1]],
    [[1, 0.5, 0], [0, 1, 0], [0, 0, 1]],
    [[1, 0, 0], [0.5, 1, 0], [0, 0, 1]],
]
matrices = [np.array(T) for T in transformations]
names = [
    "identity",
    "xy-translation",
    "x-reflection",
    "y-reflection",
    "x-scale",
    "y-scale",
    "rotate",
    "x-shear",
    "y-shear",
]
 
for i, (T, name) in enumerate(zip(matrices, names)):
    Z = A @ T.T
    ax[i // 3][i % 3].plot(A[:, 0], A[:, 1])
    ax[i // 3][i % 3].plot(Z[:, 0], Z[:, 1])
    ax[i // 3][i % 3].set_title(name)
    ax[i // 3][i % 3].grid()
 
plt.show()
 
 
 
 
 
import matplotlib.pyplot as plt
import numpy as np
import skimage
from scipy.interpolate import griddata
 
sin, cos = np.sin, np.cos
theta = np.pi / 4
 
chelsea = skimage.data.chelsea()
plt.imshow(chelsea, cmap="Blues_r")
chelsea_mean = np.mean(chelsea, axis=-1).astype(int)
 
chelsea_skip = chelsea_mean[::10, ::10]
source_values = chelsea_skip.reshape(-1)
aspect = chelsea_skip.shape[0] / chelsea_skip.shape[1]
 
x_source_space = np.linspace(0, 1, chelsea_skip.shape[1])
y_source_space = np.linspace(0, aspect, chelsea_skip.shape[0])
 
A = np.array(np.meshgrid(x_source_space, y_source_space))
 
A_image = A.reshape((2, -1)).T
plt.scatter(A_image[:, 0], -A_image[:, 1], c=source_values, cmap="Blues_r")
 
ones = np.ones(len(A_image))
A_target = np.concatenate([A_image, np.expand_dims(ones, axis=1)], axis=1)
 
T = np.array([[cos(theta), -sin(theta), 0], [sin(theta), cos(theta), 0], [0, 0, 1]])
Z = A_target @ T.T
 
plt.scatter(A_image[:, 0], -A_image[:, 1], c=source_values, cmap="Blues_r")
plt.scatter(Z[:, 0], -Z[:, 1], c=source_values, cmap="Reds_r")
 
 
XX, YY = np.mgrid[
    np.min(Z[:, 0]) : np.max(Z[:, 0]) : 512j, np.min(Z[:, 1]) : np.max(Z[:, 1]) : 512j
]
result = griddata(
    (Z[:, 0], Z[:, 1]), source_values, (XX, YY), method="cubic", fill_value=0
)
 
plt.imshow(result, cmap="Reds_r")