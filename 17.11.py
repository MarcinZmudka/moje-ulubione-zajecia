import matplotlib.pyplot as plt
import numpy as np
import random
from skimage.draw import rectangle
from skimage.transform import rotate
from scipy.signal import convolve2d, medfilt

entry_img = np.zeros((256, 256), dtype=float)

rr, cc = rectangle(start=(32, 32), end=(92, 92), shape=entry_img.shape)
entry_img[rr, cc] = 1
rr, cc = rectangle(start=(92, 92), end=(128, 128), shape=entry_img.shape)
entry_img[rr, cc] = 1
rr, cc = rectangle(start=(128, 128), end=(144, 144), shape=entry_img.shape)
entry_img[rr, cc] = 1

kat = random.uniform(-20, 20)
rot_image = rotate(entry_img, kat)

S1 = [[-1, 0, 1],
      [-2, 0, 2],
      [-1, 0, 1]]

S3 = [[1, 2, 1],
      [0, 0, 0],
      [-1, -2, -1]]

angles = np.linspace(-30, 30, 50)
abssums_s1 = np.zeros((50))
abssums_s2 = np.zeros((50))

for i in range(50):
    out_image = rotate(rot_image, angles[i])
    conved_img = convolve2d(out_image, S1)
    conved_img2 = convolve2d(out_image, S3)

    abssums_s1[i] = np.sum(np.abs(conved_img))
    abssums_s2[i] = np.sum(np.abs(conved_img2))

    fig, axs = plt.subplots(3, 2)
    axs[0, 0].imshow(entry_img)
    axs[0, 1].imshow(rot_image)
    axs[1, 0].imshow(out_image)
    axs[1, 1].imshow(conved_img)
    axs[2, 0].plot(abssums_s1)
    axs[2, 0].plot(abssums_s2)
    axs[2, 1].imshow(conved_img2)
    plt.savefig(f"foto/{i}.png")
    plt.close(fig)

idx1 = np.argmin(abssums_s1)
idx2 = np.argmin(abssums_s2)

best_angle1 = angles[idx1]
best_angle2 = angles[idx2]

best_angle = np.mean([best_angle1, best_angle2])

final_image = rotate(rot_image, best_angle)

plt.imsave("foto/baz.png", final_image)

channeled = np.stack([entry_img, final_image, rot_image], axis=2)

plt.imsave("foto/bar.png", channeled)