import numpy as np
from copy import deepcopy
import matplotlib.pyplot as plt
 
depth = 1
drange = (-1, 1)
resolution = 256
n_iter = 256
N = (2 ** depth) - 1
prober = np.sin(np.linspace(0, 8 * np.pi, resolution))
perfect_image = prober[:, np.newaxis] * prober[np.newaxis, :]
 
fig, ax = plt.subplots(figsize=(12, 8), nrows=2, ncols=3)
# plt.tight_layout()
 
# matrix_shape = (len(perfect_image), len(perfect_image))
matrix_shape = perfect_image.shape
 
n_matrix = np.zeros(matrix_shape)
o_matrix = np.zeros(matrix_shape)
 
for i in range(n_iter):
    noise = np.random.normal(0, 1, matrix_shape)
 
    n_image = perfect_image + noise
    o_image = deepcopy(perfect_image)
 
    n_dimg = (n_image - drange[0]) / (drange[1] - drange[0])
    n_dimg = np.clip(n_dimg, 0, 1)
 
    o_dimg = (o_image - drange[0]) / (drange[1] - drange[0])
    o_dimg = np.clip(o_dimg, 0, 1)
 
    dmin, dmax = (0, np.power(2, depth) - 1)
    # to samo co wczesniej wyliczone N, lol
    n_dimg = np.rint(n_dimg * dmax)
    o_dimg = np.rint(o_dimg * dmax)
 
    n_matrix += n_dimg
    o_matrix += o_dimg
 
    ax[0][0].imshow(perfect_image)
    ax[1][0].imshow(noise)
 
    ax[0][1].imshow(o_dimg, cmap="binary")
    ax[1][1].imshow(n_dimg, cmap="binary")
 
    ax[0][2].imshow(o_matrix, cmap="binary")
    ax[1][2].imshow(n_matrix, cmap="binary")
 
    # plt.savefig(f"./plots/{i}.png")