import matplotlib.pyplot as plt
import numpy as np
import random
from skimage.morphology import disk, erosion, opening, closing, dilation
# 32 x 64

img = np.zeros(shape=(32, 64))
fig, ax = plt.subplots(ncols=2, nrows=3)

for _ in range(256):
    x = random.randint(0, 64)
    y = random.randint(0, 32)
    try:
        img[y, x] = 1
        img[y+1, x] = 1
        img[y, x+1] = 1
    except:
        ...

ax[0, 0].imshow(img)
selen = disk(1)
ax[0, 1].imshow(selen)
ax[1, 0].imshow(dilation(img, selen))
ax[1, 1].imshow(erosion(img, selen))
ax[2, 0].imshow(dilation(erosion(img, selen)))
rgb = np.zeros((*img.shape, 3))
rgb[:, :, 0] = dilation(erosion(img, selen))
rgb[:, :, 1] = erosion(dilation(img, selen))
rgb[:, :, 2] = img
ax[2, 1].imshow(rgb)  # erosion(dilation(img, selen))
plt.show()
