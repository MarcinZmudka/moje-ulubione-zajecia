import skimage
import matplotlib.pyplot as plt
import numpy as np
 
img = skimage.data.chelsea()
 
fig, ax = plt.subplots(nrows=8, ncols=2)
 
chelsea_shape = img.shape
noise_shape = (*chelsea_shape, 1000)
 
noise = np.random.uniform(-512,512, noise_shape)
noised = noise + img[:,:,:, np.newaxis]
avg_noised = np.mean(noised, axis=-1).astype(int)
 
red_img = img[:,:,1]
 
identity = np.linspace(0,255,256)
identity_transformed = identity[red_img]
 
ax[0,0].plot(identity)
ax[0,1].imshow(identity_transformed)
 
negation = np.linspace(255,0,256)
negation_transformed = negation[red_img]
 
ax[1,0].plot(negation)
ax[1,1].imshow(negation_transformed)
 
threshold = np.linspace(255,255,256)
threshold[0:56] = 0
threshold[200:256] = 0
threshold_transformed = threshold[red_img]
ax[2,0].plot(threshold)
ax[2,1].imshow(threshold_transformed )
 
one_sinus = np.sin(np.linspace(0, 2*np.pi, 256))*255
one_transformed = one_sinus[red_img]
ax[3,0].plot(one_sinus)
ax[3,1].imshow(one_transformed)
 
two_sinus = np.sin(np.linspace(0, 4*np.pi, 256))*255
two_transformed = two_sinus[red_img]
ax[4,0].plot(two_sinus)
ax[4,1].imshow(two_transformed)
 
three_sinus = np.sin(np.linspace(0, 6*np.pi, 256))*255
three_transformed = three_sinus[red_img]
 
ax[5,0].plot(three_sinus)
ax[5,1].imshow(three_transformed )
 
gamma3 = np.power(identity/255, 1/3)*255
ax[6,0].plot(gamma3)
ax[6,1].imshow(gamma3[red_img])
gamma = np.power(identity/255, 3)*255
 
ax[7,0].plot(gamma)
ax[7,1].imshow(gamma[red_img])
 
plt.show()
 
 