import numpy as np
from skimage.draw import rectangle
from skimage.transform import rotate
import matplotlib.pyplot as plt
from scipy.signal import convolve2d, medfilt

img = np.zeros((256,256), dtype="float")

r1,c1 = rectangle(start=(32,32), end=(92,92), shape=img.shape)
img[r1,c1] = 1
r2,c2 = rectangle(start=(92,92), end=(128,128), shape=img.shape)
img[r2,c2] = 1
r3,c3 = rectangle(start=(128,128), end=(144,144), shape=img.shape)
img[r3,c3] = 1

angle = np.random.uniform(-20,20)
rot_img = rotate(img, angle)

S1 = [[-1,-2,-1],[0,0,0], [1,2,1]]
S3 = [[1,0,-1],[2,0,-2], [1,0,-1]]

angles = np.linspace(-30,30, 50)

abssums_s1 = np.zeros(50)
abssums_s2 = np.zeros(50)

for index, angle in enumerate(angles):
    out_img = rotate(rot_img, angle)
    conv_s1 = convolve2d(out_img, S1)
    conv_s3 = convolve2d(out_img, S3)
    abssums_s1[index] = np.sum(np.abs(conv_s1))
    abssums_s2[index] = np.sum(np.abs(conv_s3))

    fig, ax = plt.subplots(nrows=3, ncols=2)
    ax[0,0].imshow(img)
    ax[0, 0].set_title("Original")
    ax[0,1].imshow(rot_img)
    ax[0, 1].set_title("Rotated")
    ax[1,0].imshow(out_img)
    ax[1, 0].set_title("current")
    ax[1,1].imshow(conv_s1)
    ax[1, 1].set_title("s1")
    ax[2,1].imshow(conv_s3)
    ax[2, 1].set_title("s3")
    ax[2,0].plot(abssums_s1)
    ax[2,0].plot(abssums_s2)
    ax[2, 0].set_title("Original")
    plt.savefig(f"foto/{index}")


best_angle_index = np.argmin(abssums_s1)
best_angle_s1 = angles[best_angle_index+1]
best_angle_index = np.argmin(abssums_s2)
best_angle_s3 = angles[best_angle_index+1]

best_angle = np.mean([best_angle_s1, best_angle_s3])

final_img = rotate(rot_img, best_angle)
# plt.imshow(final_img)
plt.imsave("foto/baz.png", final_img)

end_img = np.array([img, final_img, rot_img])
end_img = np.stack((img, final_img, rot_img), axis=2)
plt.imsave("foto/bar.png", end_img)