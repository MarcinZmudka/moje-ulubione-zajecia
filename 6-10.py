import numpy as np
import matplotlib.pyplot as plt

linear = np.linspace(0, 4*np.pi, 8)

# print(linear)

matrix16x16 = np.zeros((16, 16))
matrix16x16[3:6, 8:12] = 1  # setting one

# print(matrix16x16[5,:])

ref = np.sin(linear)
print(linear, ref)
vec = np.linspace(1, 9, 9)
mat = ref[:, np.newaxis] * ref[np.newaxis, :]

mat2 = np.sin(linear)[:, np.newaxis] * np.sin(linear)[np.newaxis, :]
fix, ax = plt.subplots(2, 3, figsize=(10, 5))

# q
res = 1024
depth = 2
foo = np.linspace(0, np.pi*4, res)
ref = np.sin(foo)
ims = ref[:, np.newaxis] * ref[np.newaxis, :]

nrange = (-1, 1)
norm_ims = (ims - nrange[0]) / (nrange[1]-nrange[0])
norm_ims = np.clip(norm_ims, 0, 1)

dmin, dmax = (0, np.power(2, depth)-1)  # co to jest?
d_ims = np.rint(norm_ims * dmax)  # co to jest?

ax[0][0].plot(linear)
ax[1][0].imshow(ims, cmap="binary", vmin=0)
ax[1][1].imshow(norm_ims, cmap="binary", vmin=0, vmax=1)


ax[0][1].plot(ref, c="red", ls=":", lw=4, label="czerwona linia")
ax[0][1].grid(ls=":")
ax[0][1].spines["top"].set_visible(False)
ax[0][1].set_xlabel("os x")
ax[0][1].set_ylabel("os Y")
ax[1][2].imshow(d_ims, cmap="binary", vmin=dmin, vmax=dmax)
#ax[0][2].plot( linear, ref)
plt.show()

plt.tight_layout()
plt.savefig('foo.png')
