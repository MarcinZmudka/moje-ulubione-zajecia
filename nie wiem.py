import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
 
resolution = 32
 
fig, axs = plt.subplots(2,3, figsize=(10,5))
 
x = np.linspace(0, 4*np.pi, resolution)
ref = np.sin(x)
 
result_ref = ref[:,np.newaxis] * ref[np.newaxis,:]
 
# przedzialowa - odjac min i podzielic przez (max - min)
x_min, x_max = -1, 1
# x_min = result_ref.min()
# distance = result_ref.max() - result_ref.min()
def f(val):
    return ( val - x_min ) / (x_max - x_min)
 
norm_result = f(result_ref)
norm_clip = np.clip(norm_result, 0, 1)
 
depth = 2
 
dmin, dmax = 0, np.power(2, depth) - 1
digital_image = np.rint(norm_clip * dmax)
 
#
# y = np.zeros((16,16))
#
#
#
#
# y[2:5, 4:8] = 1
 
# funkcja sin sprobkowana przez foo wyplotowac do [1][0]
 
 
 
# axs[0][1].plot(ref)
# axs[0][0].plot(x)
# axs[1][0].imshow(y, cmap='binary')
# axs[0][2].plot(x, ref)
#
# for ax in axs[0]:
#     ax.grid(True)
#     ax.spines['top'].set_visible(False)
#     ax.spines['right'].set_visible(False)
#     ax.set_xlabel('os X')
#     ax.set_ylabel('os Y')
 
# mapa cieplna macierzy dwuwyrmiarowej z wektorow z axs[0][1]
# vec[:, np.linspace] * vec[np.linspace, *]
# result = x[:,np.newaxis] * x[np.newaxis,:]
 
 
 
 
axs[1][0].imshow(result_ref, cmap='binary')
axs[1][1].imshow(norm_result, vmin=0, vmax=1, cmap='binary')
axs[1][2].imshow(digital_image, vmin=dmin, vmax=dmax, cmap='binary')
plt.show()
plt.tight_layout()