import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

np.set_printoptions(precision=3, suppress=True)


def interp_nearest(x_source, y_source, x_target):
    dist = x_source[:, np.newaxis] - x_target[np.newaxis, :]
    adrr = np.argmin(np.abs(dist), axis=0)
    y_target = y_source[adrr]

    return y_target


def interp_linear(x_source, y_source, x_target):
    dist_p = x_source[:, np.newaxis] - x_target[np.newaxis, :]
    dist_n = np.copy(dist_p)

    positive = dist_p > 0
    negative = dist_n < 0

    dist_p[positive] = np.nan
    dist_n[negative] = np.nan

    addr_p = np.nanargmax(dist_p, axis=0)
    addr_n = np.nanargmin(dist_n, axis=0)

    y_p = y_source[addr_p]
    y_n = y_source[addr_n]

    x_p = x_source[addr_p]
    x_n = x_source[addr_n]

    slope = (y_p - y_n) / (x_p - x_n)

    y_target = slope * (x_target - x_n) + y_n

    y_target[0] = y_source[0]
    y_target[-1] = y_source[-1]

    return y_target


def interp_cubic(x_source, y_source, x_target):
    dist = x_source[:, np.newaxis] - x_target[np.newaxis,:]
    l = x_source[:, np.newaxis]
    p = x_source[np.newaxis,:]

    addr = np.argpartition(np.abs(dist), kth=3, axis=0)
    addr = addr[:3, :]
    x0 = x_source[addr[0]]
    x1 = x_source[addr[1]]
    x2 = x_source[addr[2]]
    y0 = y_source[addr[0]]
    y1 = y_source[addr[1]]
    y2 = y_source[addr[2]]

    l0 = (x_target - x1)*(x_target - x2)/((x0 - x1)*(x0 - x2))
    l1 = (x_target - x0)*(x_target - x2)/((x1 - x0)*(x1 - x2))
    l2 = (x_target - x0)*(x_target - x1)/((x2 - x0)*(x2 - x1))
    y_target = y0*l0 + y1*l1 + y2*l2
    return y_target


# Probers
original_prober = np.sort(np.random.uniform(size=8)*np.pi*4)
target_prober = np.linspace(np.min(original_prober),
                            np.max(original_prober), 32)

# Sampling
original_signal = np.sin(original_prober)

# Out-of-box interpolators
fn = interp1d(original_prober, original_signal, kind='nearest')
fl = interp1d(original_prober, original_signal, kind='linear')
fc = interp1d(original_prober, original_signal, kind='quadratic')

# Interpolation
target_signal_fn = fn(target_prober)
target_signal_fl = fl(target_prober)
target_signal_fc = fc(target_prober)

args = (original_prober, original_signal, target_prober)
own_target_signal_fn = interp_nearest(*args)
own_target_signal_fl = interp_linear(*args)
own_target_signal_fc = interp_cubic(*args)

# Store them for plotting
target_signals = [target_signal_fn, target_signal_fl, target_signal_fc]
own_target_signals = [own_target_signal_fn,
                      own_target_signal_fl, own_target_signal_fc]

# Plotting
fig, ax = plt.subplots(4, 1, figsize=(8, 8*1.618))

ax[0].scatter(original_prober, np.ones_like(original_prober) * -.5,
              label='origin', c='black')
ax[0].scatter(target_prober, np.ones_like(target_prober) * .5,
              label='target', c='red')
ax[0].plot(original_prober, original_signal, c='black')
ax[0].set_ylim(-1.5, 1.5)
ax[0].legend(frameon=False, loc=9, ncol=2)
ax[0].set_yticks([])
ax[0].set_xticks(original_prober)
ax[0].set_xticklabels([])
ax[0].spines['top'].set_visible(False)
ax[0].spines['right'].set_visible(False)
ax[0].spines['left'].set_visible(False)
ax[0].grid(ls=":")


for i, (target_signal, own_target_signal) in enumerate(zip(target_signals,
                                                           own_target_signals)):
    ax[1+i].plot(original_prober, original_signal, c='black', ls=":")
    ax[1+i].plot(target_prober, target_signal, 'red', ls=":")
    ax[1+i].plot(target_prober, own_target_signal, 'red')
    ax[1+i].grid(ls=":")
    ax[1+i].set_xticks(target_prober)
    ax[1+i].set_xticklabels([])
    ax[1+i].spines['top'].set_visible(False)
    ax[1+i].spines['right'].set_visible(False)

ax[1].set_title('neighbor')
ax[2].set_title('linear')
ax[3].set_title('cubic')

plt.savefig('foo.png')
